<?php


namespace App\Services;


use App\Dto\RequestDto;
use App\Interfaces\OpenAiInterface;
use App\Interfaces\TelegramBotInterface;
use Spatie\DataTransferObject\DataTransferObject;

/**
 * Class OpenAiService
 * @package App\Services
 */
class OpenAiService
{
    /**
     * @var string
     */
    private string $text;

    /**
     * @var string
     */
    private string $chatId;

    /**
     * OpenAiService constructor.
     * @param OpenAiInterface $openAi
     * @param TelegramBotInterface $telegram
     */
    public function __construct(
        private OpenAiInterface $openAi,
        private TelegramBotInterface $telegram)
    {}

    /**
     * @param RequestDto $dto
     * @return $this
     */
    public function setRequest(DataTransferObject $dto): self
    {
        $this->text = $dto->text;
        $this->chatId = $dto->chatId;
        return $this;
    }

    /**
     * @return string
     */
    public function getAnswer(): string
    {
       return $this->telegram->sendMessage($this->getComplete()->choices[0]->text, $this->chatId);
    }

    /**
     * @return object
     */
    private function getComplete(): object
    {
        return json_decode($this->openAi->send($this->text));
    }
}
