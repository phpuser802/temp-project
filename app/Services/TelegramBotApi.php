<?php


namespace App\Services;


use App\Interfaces\TelegramBotInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Class TelegramBotApiConnector
 * @package App\Components
 */
class TelegramBotApi implements TelegramBotInterface
{
    /**
     * TelegramBotApi constructor.
     * @param string $botToken
     * @param Client $client
     */
    public function __construct(
        private string $botToken,
        private Client $client,
    ){}

    /**
     * @param string $text
     * @param string $chatId
     * @return string
     * @throws GuzzleException
     */
    public function sendMessage(string $text, string $chatId): string
    {
        $this->send('sendMessage', [
            'text' => $text,
            'chat_id' => $chatId,
        ]);

        return $text;
    }

    /**
     * @param $url
     * @return string
     * @throws GuzzleException
     */
    public function setWebhook($url): string
    {
        return $this->send('setWebHook', ['url' => $url]);
    }

    /**
     * @param string $method
     * @param array $params
     * @return string
     * @throws GuzzleException
     */
    private function send(string $method, array $params): string
    {
        $uri = '/bot' . $this->botToken . '/' . $method;
        return $this->client->post($uri, ["form_params" => $params])->getBody()->getContents();
    }
}
