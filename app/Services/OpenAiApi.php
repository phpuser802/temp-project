<?php


namespace App\Services;


use App\Interfaces\OpenAiInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Class OpenAiApi
 * @package App\Services
 */
class OpenAiApi implements OpenAiInterface
{

    /**
     * OpenAiApi constructor.
     * @param Client $client
     */
    public function __construct(private Client $client)
    {}

    /**
     * @param string $text
     * @return string
     * @throws GuzzleException
     */
    public function send(string $text) : string
    {
        $uri = '/v1/completions';
        return $this->client->post($uri, [
            "json" => [
                'model' => 'text-davinci-003',
                'prompt' => $text,
                'temperature' => 0.1,
                'max_tokens' => 1000,
                'frequency_penalty' => 0.1,
                'presence_penalty' => 0.0,
                'top_p' => 1,
            ]
        ])->getBody()->getContents();
    }
}
