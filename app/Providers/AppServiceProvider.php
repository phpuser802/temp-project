<?php

namespace App\Providers;

use App\Interfaces\OpenAiInterface;
use App\Interfaces\TelegramBotInterface;
use App\Services\OpenAiApi;
use App\Services\TelegramBotApi;
use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(OpenAiInterface::class, function ($app){
            return $app->makeWith(OpenAiApi::class, [
                'client' => $app->makeWith(Client::class, [
                    'config' => [
                        'base_uri' => config('openai.apiUrl'),
                        'headers' => [
                            'Content-Type' => 'application/json',
                            'Authorization' => 'Bearer ' . config('openai.key')
                        ],
                    ],
                ]),
            ]);
        });

        $this->app->bind(TelegramBotInterface::class, function ($app){
            return $app->makeWith(TelegramBotApi::class, [
                'botToken' => config('telegram.botToken'),
                'client' => $app->makeWith(Client::class, [
                    'config' => [
                        'base_uri' => config('telegram.apiUrl'),
                    ],
                ]),
            ]);
        });
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
    }
}
