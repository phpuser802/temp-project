<?php

namespace App\Http\Controllers;

use App\Dto\RequestDto;
use App\Http\Requests\TelegramMessageRequest;
use App\Jobs\OpenAiServiceJob;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\App;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ChatGptExampleWebhookController
 * @package App\Http\Controllers
 */
class ChatGptExampleWebhookController extends Controller
{
    /**
     * Handle telegram webhook.
     *
     * @param TelegramMessageRequest $request
     * @return JsonResponse
     */
    public function index(TelegramMessageRequest $request): JsonResponse
    {
        OpenAiServiceJob::dispatch(
            App::makeWith(RequestDto::class, ['args' => $request->validated()])
        );

        return new JsonResponse(['success' => true], Response::HTTP_OK);
    }
}
