<?php

namespace App\Console\Commands;

use App\Interfaces\TelegramBotInterface;
use Illuminate\Console\Command;

/**
 * Class setWebhook
 * @package App\Console\Commands
 */
class setWebhook extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'setWebhook {url}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set Webhook for telegram bot';

    /**
     * Execute the console command.
     *
     * @param TelegramBotInterface $telegram
     */
    public function handle(TelegramBotInterface $telegram): void
    {
        echo $telegram->setWebhook($this->argument('url')) . "\n";
    }
}
