<?php


namespace App\Interfaces;


/**
 * Interface OpenAiInterface
 * @package App\Interfaces
 */
interface OpenAiInterface
{
    /**
     * @param string $text
     * @return string
     */
    public function send(string $text): string;
}
