<?php


namespace App\Interfaces;

/**
 * Interface TelegramBotInterface
 * @package App\Interfaces
 */
interface TelegramBotInterface
{
    /**
     * @param string $text
     * @param string $chatId
     */
    public function sendMessage(string $text, string $chatId): string;

    /**
     * @param $url
     * @return string
     */
    public function setWebhook($url): string;
}
