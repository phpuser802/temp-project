<?php


namespace App\Dto;


use Spatie\DataTransferObject\Attributes\MapFrom;
use Spatie\DataTransferObject\DataTransferObject;

/**
 * Class RequestDto
 * @package App\Dto
 */
class RequestDto extends DataTransferObject
{
    /**
     * @var string
     */
    #[MapFrom('message.text')]
    public string $text;

    /**
     * @var int
     */
    #[MapFrom('message.chat.id')]
    public int $chatId;
}
