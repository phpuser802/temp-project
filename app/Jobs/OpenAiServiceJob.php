<?php

namespace App\Jobs;


use App\Services\OpenAiService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Spatie\DataTransferObject\DataTransferObject;

class OpenAiServiceJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @param DataTransferObject $request
     * @return void
     */
    public function __construct(
        private DataTransferObject $request
    ){}

    /**
     * Execute the job.
     *
     * @param OpenAiService $service
     * @return void
     */
    public function handle(OpenAiService $service): void
    {
       $service->setRequest($this->request)->getAnswer();
    }
}
