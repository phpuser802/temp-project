<?php


return [
    "key" => env('OPENAI_API_KEY'),
    "apiUrl" => 'https://api.openai.com',
];
