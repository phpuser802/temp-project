<?php


return [
    "botToken" => env('BOT_TOKEN'),
    "apiUrl" => 'https://api.telegram.org',
];
